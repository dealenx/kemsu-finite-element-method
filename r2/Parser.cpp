#include "Parser.h"

using namespace std;

Parser::Parser()
{
}


Parser::~Parser()
{
}

vector<string> Parser::Extract(const string& stoextract) {
	vector<string> aListofWords;
	stringstream sstoext(stoextract);
	string sWordBuf;

	while (sstoext >> sWordBuf)
		aListofWords.push_back(sWordBuf);

	return aListofWords;
}

void Parser::read(std::string path)
{
	vector<string> tmpStr;
	stringstream str;
	setlocale(LC_CTYPE, "rus");

	string line;

	double *list1_col_x;
	double *list1_col_y;
	int *list1_col_flag;
	int *list1_col_index;

	int N;
	

	int *list2_col_point1;
	int *list2_col_point2;
	int *list2_col_point3;
	int *list2_col_index;

	int M;

	ifstream in(path); // �������� ���� ��� ������
	if (in.is_open()) {
		getline(in, line);
		cout << line << endl;

		tmpStr = Extract(line);

		N = atoi(tmpStr[0].c_str());
		M = atoi(tmpStr[1].c_str());

		points.resize(N);
		elemenets.resize(M);

		cout << "N: " << N << endl;
		cout << "M: " << M << endl;

		list1_col_x = new double[N];
		list1_col_y = new double[N];
		list1_col_flag = new int[N];
		list1_col_index = new int[N];

		list2_col_point1 = new int[M];
		list2_col_point2 = new int[M];
		list2_col_point3 = new int[M];
		list2_col_index = new int[M];

		for (int i = 0; i < N; i++) {
			vector<string> tmplocStr;
			getline(in, line);

			tmplocStr = Extract(line);

			list1_col_x[i] = strtod(tmplocStr[0].c_str(), NULL);
			list1_col_y[i] = strtod(tmplocStr[1].c_str(), NULL);

			list1_col_flag[i] = atoi(tmplocStr[2].c_str());
			list1_col_index[i] = atoi(tmplocStr[3].c_str());
			points[i].init(list1_col_x[i], list1_col_y[i], list1_col_flag[i], list1_col_index[i] - 1);
		}
		Point *tempPoint1 = new Point();
		Point *tempPoint2 = new Point();
		Point *tempPoint3 = new Point();
		for (int i = 0; i < M ; i++) {
			vector<string> tmplocStr;
			getline(in, line);

			tmplocStr = Extract(line);
			
			list2_col_point1[i] = atoi(tmplocStr[0].c_str()) - 1;

			list2_col_point2[i] = atoi(tmplocStr[1].c_str()) - 1;
			list2_col_point3[i] = atoi(tmplocStr[2].c_str()) - 1;
			list2_col_index[i] = atoi(tmplocStr[3].c_str()) - 1;

			tempPoint1->init(points[list2_col_point1[i]].x, points[list2_col_point1[i]].y, points[list2_col_point1[i]].flag, points[list2_col_point1[i]].index);
			tempPoint2->init(points[list2_col_point2[i]].x, points[list2_col_point2[i]].y, points[list2_col_point2[i]].flag, points[list2_col_point2[i]].index);
			tempPoint3->init(points[list2_col_point3[i]].x, points[list2_col_point3[i]].y, points[list2_col_point3[i]].flag, points[list2_col_point3[i]].index);
			elemenets[i].init( list2_col_index[i], tempPoint1, tempPoint2, tempPoint3);
		}
	}
	in.close();     // ��������� ����
		
	cout << "End of parser" << endl;
}
