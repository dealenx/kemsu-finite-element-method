#pragma once
#include <vector> 
#include <stdlib.h>
#include "Point.h";

using namespace std;

class Element
{
public:
	int index;
	vector<Point> points;
	double localA[3][3];
	Element();
	Element(int index, Point * point1, Point *point2, Point *point3);
	void init(int index, Point * point1, Point *point2, Point *point3);
	void calcLocalA();
	~Element();
};

