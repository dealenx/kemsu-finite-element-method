#include "Element.h"
#include <iostream>

using namespace std;

Element::Element()
{

}
Element::Element(int index, Point * point1, Point *point2, Point *point3)
{
	init(index, point1, point2, point3);
}
void Element::init(int index, Point * point1, Point *point2, Point *point3)
{
	this->index = index;
	points.resize(3);
	points[0].init(point1->x, point1->y, point1->flag, point1->index);
	points[1].init(point2->x, point2->y, point2->flag, point2->index);
	points[2].init(point3->x, point3->y, point3->flag, point3->index);
	calcLocalA();
}

void Element::calcLocalA() {
	double *a = new double[3];
	double *b = new double[3];

	double *x = new double[3];
	double *y = new double[3];	
	for (int i = 0; i < 3; i++) {
		x[i] = points[i].x;
		y[i] = points[i].y;
	}
	a[0] = x[2] - x[1];
	a[1] = x[0] - x[2];
	a[2] = x[1] - x[0];

	b[0] = y[1] - y[2];
	b[1] = y[2] - y[0];
	b[2] = y[0] - y[1];


	double S = abs( (b[0] * a[1] ) - (b[1] * a[0]) );

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			localA[i][j] = ( b[i]*b[j] + a[i]*a[j] ) / (2 * S);
		} 
	}
	/*
	localA[1][0] = localA[0][1];
	localA[2][0] = localA[0][2];
	localA[2][1] = localA[1][2];
	*/
	if (this->index == 2) {
		for (int i = 0; i < 3; i++) {
			cout << "x[i]: " << x[i] << endl;
			cout << "y[i]: " << y[i] << endl;
			cout << "a[i]: " << a[i] << endl;
			cout << "b[i]: " << b[i] << endl;
			cout << "S: " << S << endl;
		}
		
		cout << "A local: " << endl;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				cout << localA[i][j] << " ";
			} cout << endl;
		}
	}
}


Element::~Element()
{
}
