#include "Math.h"

#include <fstream>

Math::Math(vector < Element> p_elements, vector < Point> p_points)
{
	init(p_elements, p_points);
	calc(); 
	
}


Math::~Math()
{
}

void Math::init(vector < Element> p_elements, vector < Point> p_points)
{
	points = p_points;
	elements = p_elements;
	n = p_points.size();
	//cout << "TEST: " << elements[2].index << endl;
	//cout << "n: " << n << endl;
	x = new double[n];
	phi = new double[n];
	phi_an = new double[n];
	rightPart = new double[n];
	/*a = 0;
	b = 1;
	h = (b - a) / (n - 1);
	x[0] = a;
	x[n - 1] = b;

	for (int i = 0; i < n; i++) {
		x[i] = a + i * h;
		
	}
	for (int i = 0; i < n; i++) {
		phi_an[i] = anFunc(x[i]);
	}
	
	*/
	/*
	phi[0] = 0;
	phi[n - 1] = 1;

	A = new double*[n];
	for (int i = 0; i < n; i++)
	{
		A[i] = new double[n];
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			A[i][j] = 0;
		}
	}

	K = new double*[2];
	for (int i = 0; i < 2; i++) {
		K[i] = new double[2];
	}

	//��������
	K[0][0] = (1.0 / h) + (h / 3);
	K[0][1] = -(1.0 / h) + (h / 6);
	K[1][0] = -(1.0 / h) + (h / 6);
	K[1][1] = (1.0 / h) + (h / 3);

	for (int k = 0; k < n - 1; k++) {
		A[0 + k][0 + k] += K[0][0];
		A[0 + k][1 + k] += K[0][1];
		A[1 + k][0 + k] += K[1][0];
		A[1 + k][1 + k] += K[1][1];
	}
	for (int i = 0; i < n; i++) {
		A[0][i] = 0;
		A[n - 1][i] = 0;
	}
	A[0][0] = 1;
	A[n - 1][n - 1] = 1;
	for (int i = 0; i < n; i++) {
		rightPart[i] = 0;
	}*/
	//printA();
	/*cout << endl;
	setDirichlet(0, phi[0]);
	setDirichlet(n-1, phi[n - 1]);*/
	//printA();


	A = new double*[n];
	for (int i = 0; i < n; i++)
	{
		A[i] = new double[n];
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			A[i][j] = 0;
		}
	}

	cout <<  "Writing in matrixA.txt";
	int column_index;
	int row_index;
	for (int e = 0; e < elements.size(); e++) {
		for (int i = 0; i < 3; i++) {
			row_index = elements[e].points[i].index;
			for (int j = 0; j < 3; j++) {
				column_index = elements[e].points[j].index;
				A[row_index][column_index] += elements[e].localA[i][j];
			}
		}
	}
	cout << "End Writing";
	/*
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << A[i][j] << " ";
		} cout << endl;
	}*/

	ofstream fout("matrixA.txt", ios::out | ios::trunc); // ������ ������ ������ ofstream ��� ������ � ��������� ��� � ������ cppstudio.txt
	
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			fout << A[i][j] << "\t";
		} fout << endl;
	}
	fout.close(); // ��������� ����




}


void Math::calc()
{
	//������������� �����))0)
	//BISC_Mod(A, rightPart, phi, n);
	
	
	/*
	cout << "phi: " << endl;
	printVector(phi, n);
	
	cout << "phi_an: " << endl;
	printVector(phi_an, n);
	*/
	/*for (int i = 0; i < n; i++) {
		cout << "phi=" << phi[i] << " phi_an=" << phi_an[i] << " app=" << abs(phi[i] - phi_an[i]) << endl;
	}*/
}

void Math::printA()
{
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << A[i][j] << " ";
		} cout << endl;
	}
}

void Math::printVector(double *arr, int n)
{
	for (int i = 0; i < n; i++) {
			cout << arr[i] << " ";
	}
	cout << endl;
}

double Math::anFunc(double x)
{
	double res = (exp(x) - exp(-x)) / (exp(1.0) - exp(-1.0));
	return res;
}

void Math::setDirichlet(int num, double val)
{
	for (int i = 0; i < n; i++) {
		if (i != num) {
			A[num][i] = 0;
		}
	}
	
	A[num][num] = 1.0;
	rightPart[num] = val;
	

	for (int i = 0; i < n; i++) {
		if (i != num) {
			rightPart[i] = -A[i][num] * val;
			A[i][num] = 0;

		}
	}
}


//������� ����������
int Math::BISC_Mod(double ** AA, double * B, double * X, int n)
{
	double *Rn, *_Rn, *Pn, *Pn1, *Vn, *A, eps = 10e-10, *Sn, *Tn;
	int iter = 0;
	Rn = new double[n];
	Pn = new double[n];
	Pn1 = new double[n];
	_Rn = new double[n];
	Vn = new double[n];
	A = new double[n];
	Sn = new double[n];
	Tn = new double[n];

	Methods::null(A, n);

	for (int i = 0; i < n; i++)
	{
		X[i] = B[i] + 0.000001;
	}


	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			A[i] += AA[i][j] * X[j];  //???????????? ? ?? 1?? ??????????? ??????? ?=1
		}
		_Rn[i] = Rn[i] = B[i] - A[i];
	}

	Methods::null(Pn, n);
	Methods::null(Vn, n);

	double pn = 1.;
	double an = 1.;
	double bn = 1.;
	double wn = 1.;
	double pn1;
	double norm = 0;

	int p = 1;
	while (true)
	{
		pn1 = Methods::scalar(_Rn, Rn, n);
		//
		if (pn1 == 0)
		{
			//cout << "ERROR!" << endl;
		}
		if (p == 1)
		{
			for (int i = 0; i < n; i++)
			{
				Pn1[i] = Rn[i];
			}
		}
		else
		{
			bn = (pn1 / pn)*(an / wn);
			for (int i = 0; i < n; i++)
			{
				Pn1[i] = Rn[i] + bn * (Pn[i] - wn * Vn[i]);
			}
		}
		//
		Methods::null(Vn, n);
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				Vn[i] += AA[i][j] * Pn1[j];
			}
		}

		an = pn1 / Methods::scalar(_Rn, Vn, n);
		for (int i = 0; i < n; i++)
		{
			Sn[i] = Rn[i] - an * Vn[i];
		}
		if ((sqrt(Methods::scalar(Sn, Sn, n))) <= eps)
		{
			for (int i = 0; i < n; i++)
			{
				X[i] += an * Pn1[i];
			}
			//cout << "iter = " << iter << endl;
			delete[]Rn;
			delete[]Pn;
			delete[]Pn1;
			delete[]_Rn;
			delete[]Vn;
			delete[]A;
			delete[]Sn;
			delete[]Tn;

			return 1;
		}
		else
		{
			Methods::null(Tn, n);
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					Tn[i] += AA[i][j] * Sn[j];
				}
			}

			wn = Methods::scalar(Tn, Sn, n) / Methods::scalar(Tn, Tn, n);
			for (int i = 0; i < n; i++)
			{
				X[i] += an * Pn1[i] + wn * Sn[i];
				Rn[i] = Sn[i] - wn * Tn[i];
			}

			pn = pn1;
			for (int i = 0; i < n; i++)
			{
				Pn[i] = Pn1[i];
			}

			if ((sqrt(Methods::scalar(Rn, Rn, n))) <= eps)
			{
				//cout << "iter = " << iter << endl;
				delete[]Rn;
				delete[]Pn;
				delete[]Pn1;
				delete[]_Rn;
				delete[]Vn;
				delete[]A;
				delete[]Sn;
				delete[]Tn;

				return 1;
			}
		}
		p++;
		iter++;
	}
}
