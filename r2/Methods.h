#pragma once
#include "math.h"
#include <vector>

using namespace std;

class Methods
{
public:
	static void null(double ** a, int n);
	static void null(double * a, int n);
	static void testNull(vector<double *> a, int n);
	static void testNull(vector<double **> a, int n);
	static void multMV(double ** a, double * b, double * c, int n);
	static void multMC(double ** a, double c, int n);
	static void mulVC(double *a, double c, int n);
	static void actionsVC(double *a, double c, int n, char act);
	static void equateV(double *a, double *b, int n);
	static void equateVC(double *a, double c, int n);
	static double maxElement(double *a, int n);
	static double calcMistake(double *a, double *b, int n);
	static double scalar(double *a, double *b, int n);
	static double fingLength(double x1, double x2, double y1, double y2);
	static double fillRange(double *x, double a, double b, int n);
	Methods();
	~Methods();
};