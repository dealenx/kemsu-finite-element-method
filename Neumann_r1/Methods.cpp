#include "./Methods.h"

void Methods::multMV(double **a, double *b, double *c, int n)
{
	for (int i = 0; i<n; i++) {
		c[i] = 0;
		for (int j = 0; j<n; j++) {
			c[i] += (a[i][j] * b[j]);
		}
	}
}

void Methods::multMC(double ** a, double c, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			a[i][j] *= c;
		}
	}
}

void Methods::mulVC(double * a, double c, int n)
{
	for (int i = 0; i < n; i++)
	{
		a[i] *= c;
	}
}

void Methods::null(double *a, int n)
{
	for (int i = 0; i<n; i++)
		a[i] = 0;
}

void Methods::testNull(vector<double*> a, int n)
{
	for (unsigned int k = 0; k < a.size(); k++)
	{
		for (int i = 0; i < n; i++)
		{
			a[k][i] = 0;
		}
	}
}

void Methods::testNull(vector<double**> a, int n)
{
	for (unsigned int k = 0; k < a.size(); k++)
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				a[k][i][j] = 0;
			}
		}
	}
}

double Methods::maxElement(double * a, int n)
{
	double max = a[0];
	for (int i = 1; i < n; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
		}
	}
	return max;
}

double Methods::calcMistake(double * a, double * b, int n)
{
	double *error = new double[n];
	double tmp;

	for (int i = 0; i < n; i++)
	{
		error[i] = abs(a[i] - b[i]);
	}

	tmp = abs(maxElement(error, n));

	delete[]error;
	error = NULL;

	return tmp;
}

double Methods::scalar(double * a, double * b, int n)
{
	double s = 0;
	for (int i = 0; i < n; i++)
	{
		s += a[i] * b[i];
	}
	return s;
}

void Methods::equateV(double * a, double * b, int n)
{
	for (int i = 0; i < n; i++)
	{
		a[i] = b[i];
	}
}

void Methods::null(double **a, int n)
{
	for (int i = 0; i<n; i++)
		for (int j = 0; j<n; j++)
			a[i][j] = 0;
}

void Methods::equateVC(double * a, double c, int n)
{
	for (int i = 0; i < n; i++)
	{
		a[i] = c;
	}
}

double Methods::fingLength(double x1, double x2, double y1, double y2)
{
	return sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
}

double Methods::fillRange(double * x, double a, double b, int n)
{
	double h = (b - a) / (n - 1);

	for (int i = 0; i < n; i++)
	{
		x[i] = a + i * h;
	}

	return h;
}

void Methods::actionsVC(double * a, double c, int n, char act)
{
	switch (act)
	{
	case '+':
		for (int i = 0; i < n; i++)
		{
			a[i] += c;
		}
		break;
	case '-':
		for (int i = 0; i < n; i++)
		{
			a[i] -= c;
		}
		break;
	case '*':
		for (int i = 0; i < n; i++)
		{
			a[i] *= c;
		}
		break;
	case '/':
		for (int i = 0; i < n; i++)
		{
			a[i] /= c;
		}
		break;
	default:
		break;
	}
}

Methods::Methods()
{
}


Methods::~Methods()
{
}
