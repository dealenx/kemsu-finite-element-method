#pragma once
#include <iostream>
#include "Methods.h"
#include <math.h>
using namespace std;

class Math
{
public:
	double h;
	int n;
	double *x;
	double *phi;
	double *phi_an;
	double *rightPart;
	double a;
	double b;
	double **K; 
	double **A;

	Math();
	~Math();
	void init();
	void calc();
	void printA(); 
	void setDirichlet(int num, double val);
	int BISC_Mod(double ** AA, double * B, double * X, int n);
	void printVector(double *arr, int n);
	double anFunc(double x);
};

