#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <clocale>
#include <sstream>
#include <vector> 
#include <stdlib.h>

#include "Point.h"
#include "Element.h"

using namespace std;

class Parser
{ 
public:
	Parser();
	~Parser();

	vector < Point> points;
	vector < Element> elemenets;

	vector<string> Extract(const string& stoextract);
	void read(string path);
};

